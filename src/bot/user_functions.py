import re

import spacy

from nltk import pos_tag as pt
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize


pos_tag = pt

word2vec = spacy.load("en_core_web_lg")
stop_words = set(stopwords.words("english"))


def preprocess(input_sentence: str) -> list:
    input_sentence = input_sentence.lower()
    input_sentence = re.sub(r"[^\w\s]", "", input_sentence)
    tokens = word_tokenize(input_sentence)

    input_sentence = [token for token in tokens if not token in stop_words]

    return input_sentence


def compare_overlap(user_message, possible_response):
    similar_words = 0

    for token in user_message:
        similar_words += 1 if token in possible_response else 0

    return similar_words


def extract_nouns(tagged_message):
    return [token[0] for token in tagged_message if token[1].startswith("N")]


def compute_similarity(tokens, category):
    return [[token.text, category.text, token.similarity(category)] for token in tokens]


if __name__ == "__main__":
    print("You can't run this script directly, import the functions to use them")
