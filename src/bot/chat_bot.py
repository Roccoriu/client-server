from collections import Counter
from langcodes import Language

import src.bot.user_functions as uf
from src.bot import responses as res


class ChatBot:
    def __init__(
        self,
        exit_commands: tuple = res.DEFAULT_EXIT_COMMANDS,
        responses: list[str] = res.RESPONSES,
        blank_spot: str = res.BLANK_SPOT,
        word2vec: Language = res.WORD2VEC,
    ):
        self.exit_commands = exit_commands
        self.responses = responses
        self.blank_spot = blank_spot
        self.word2vec = word2vec

    def make_exit(self, user_message: str) -> bool:
        for command in self.exit_commands:
            if command in str(user_message):
                print("goodbye, see you soon")
                return True

        return False

    def find_intent_match(self, responses: list[str], user_message: str) -> str:
        processed_user_msg = Counter(uf.preprocess(user_message))

        similarity_list = [
            uf.compare_overlap(processed_user_msg, Counter(uf.preprocess(res)))
            for res in responses
        ]

        res_index = similarity_list.index(max(similarity_list))
        return responses[res_index]

    def find_entities(self, user_message: str) -> str:
        tagged_msg = uf.pos_tag(uf.preprocess(user_message))
        msg_nouns = str().join(uf.extract_nouns(tagged_msg))

        tokens = self.word2vec(msg_nouns)
        category = self.word2vec(self.blank_spot)

        word2veg_res = uf.compute_similarity(tokens, category)
        word2veg_res.sort(key=lambda x: x[2])

        return word2veg_res[-1][1] if len(word2veg_res) >= 1 else self.blank_spot

    def respond(self, user_message: str) -> str:
        best_response = self.find_intent_match(self.responses, user_message)

        entity = self.find_entities(user_message)

        print(best_response.format(entity))

        return str(input("What else can I do for you? "))

    def chat(self) -> any:
        user_message = str(input("Welcome, what can I do for you? "))

        while not self.make_exit(user_message):
            user_message = self.respond(user_message)


if __name__ == "__main__":
    print("run this script via poetry run cli chat-bot chat")
