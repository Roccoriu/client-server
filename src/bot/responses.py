import sys
import json

from pathlib import Path

import spacy

RESPONSES = None
WORD2VEC = spacy.load("en_core_web_lg")
DEFAULT_EXIT_COMMANDS = ("exit", "quit")

responses_file = f"{Path(__file__).resolve().parent}/assets/responses.json"

try:
    with open(responses_file, encoding="utf-8") as f:
        responses = json.load(f)

except json.JSONDecodeError as err:
    print(f"failed to decode {responses_file}, {err}")
    sys.exit(9)

except FileNotFoundError as err:
    print(
        f"file {responses_file} not found, plesae make sure the file exists in that direcotry"
    )
    sys.exit(10)


RESPONSES = responses["responses"]
BLANK_SPOT = responses["blankSpot"]
DEFAULT_EXIT_COMMANDS = responses["exitCommands"]
