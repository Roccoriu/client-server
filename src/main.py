#!/usr/bin/env python3


import typer

from src.cli import chat_bot, server, client



app = typer.Typer()


app.add_typer(server.app, name="server")
app.add_typer(client.app, name="client")
app.add_typer(chat_bot.app, name="chat-bot")


if __name__ == "__main__":
    app()
