import os
import socket
from dotenv import load_dotenv

load_dotenv()


def main() -> None:
    print("==========================")
    print(f'Listening on {os.environ.get("IP")} {os.environ.get("PORT")}')
    print("==========================")

    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.bind((os.environ.get("IP"), int(os.environ.get("PORT"))))

    tcp.listen(1)

    connect, address = tcp.accept()
    print(f"Connection from {address}")

    connect.send(bytearray(f"Hi client: {address}", "utf-8"))
    connect.close()


if __name__ == "__main__":
    print("run it with typer")
