import os
import typer

import nltk

from src.bot.chat_bot import ChatBot

app = typer.Typer()


@app.command()
def download_models():
    nltk.download("stopwords")
    nltk.download("punkt")
    nltk.download("averaged_perceptron_tagger")
    os.system("python -m spacy download en_core_web_lg")


@app.command()
def chat():
    bot = ChatBot()
    bot.chat()


if __name__ == "__main__":
    print("Run poetry run cli chat-bot --help to see available commands ")
