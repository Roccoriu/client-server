import typer

from src.server.server import main

app = typer.Typer()


@app.command()
def run():
    main()


if __name__ == "__main__":
    print("run using main.py")
