import socket


def main() -> None:
    ip_addr = str(input("Please enter the server's IP: "))
    port = int(input("Please enter the server's port: "))

    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.connect((ip_addr, port))
    print(f"Successfull connection with: {ip_addr}:{port}")

    load = " " * 30
    buffer = bytearray(load, "utf-8")
    tcp.recv_into(buffer)
    print(buffer.decode())

    tcp.close()


if __name__ == "__main__":
    print("run it with typer")
