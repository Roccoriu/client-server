# Repo for Client Server Architecture Course
This repo contains the files for the chat_bot exercise and websockets demonstartion. The README filecontains instructions for the installation of the necessary dependencies.

## Requirements for running the project

To run the project you need the following prerequisites:

- python `3.10.0 or newer`
- poetry `1.2.0 or newer`

```bash
# install dependencies using poetry
poetry install --with dev

# once installed open a shell with poetry
poetry shell

# using this shell install the necessary language packs and models
poetry run cli chat-bot download-models

# now you can use the bot
poetry run cli chat-bot chat

```

## Change Responses

To Change the responses you can simply edit the responses section in the responses_file.json file found in the assets folder

```json
"responses": [
    ...
    "My new response"
],

```
